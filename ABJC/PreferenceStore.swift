//
//  PreferenceStore.swift
//  ABJC
//
//  Created by Noah Kamara on 26.03.21.
//

import Foundation

import Foundation
import Combine
import SwiftUI

public class PreferenceStore: ObservableObject {
    private enum Keys {
        static let watchnowtab = "tabviewconfiguration.watchnowtab"
        static let seriestab = "tabviewconfiguration.seriestab"
        static let moviestab = "tabviewconfiguration.moviestab"
        static let searchtab = "tabviewconfiguration.searchtab"
        
        static let showsTitles = "design.showstitles"
        static let debugMode = "debugmode"
        static let betaflags = "betaflags"
    }
    
    private let cancellable: Cancellable
    private let defaults: UserDefaults
    
    public let objectWillChange = PassthroughSubject<Void, Never>()
    
    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
        
        // Load Beta Flags
        let data = defaults.object(forKey: Keys.betaflags) as? [String] ?? [String]()
        let flags = Set(data.filter({ BetaFlag(rawValue: $0) != nil }).map({BetaFlag(rawValue: $0)!}))
        
        defaults.register(defaults: [
            Keys.watchnowtab: false,
            Keys.moviestab: true,
            Keys.seriestab: true,
            Keys.searchtab: true,
            Keys.debugMode: false,
            Keys.showsTitles: flags.contains(.showsTitles),
            Keys.betaflags: []
        ])
        
        cancellable = NotificationCenter.default
            .publisher(for: UserDefaults.didChangeNotification)
            .map { _ in () }
            .subscribe(objectWillChange)
    }
    
    
    /// Current client version
    public let version: Version = Version()
    
    
    /// Wether to show the "Watch Now" Tab
    public var showsWatchNowTab: Bool {
        get { defaults.bool(forKey: Keys.watchnowtab) }
        set { defaults.setValue(newValue, forKey: Keys.watchnowtab) }
    }
    
    /// Wether to show the "Movies" Tab
    public var showsMoviesTab: Bool {
        get { defaults.bool(forKey: Keys.moviestab) }
        set { defaults.setValue(newValue, forKey: Keys.moviestab) }
    }
    
    /// Wether to show the "TV Shows" Tab
    public var showsSeriesTab: Bool {
        get { defaults.bool(forKey: Keys.seriestab) }
        set { defaults.setValue(newValue, forKey: Keys.seriestab) }
    }
    
    /// Wether to show the "Search" Tab
    public var showsSearchTab: Bool {
        get { defaults.bool(forKey: Keys.searchtab) }
        set { defaults.setValue(newValue, forKey: Keys.searchtab) }
    }
    
    /// Wether to always show titles for media items
    public var showsTitles: Bool {
        get { defaults.bool(forKey: Keys.showsTitles) }
        set { defaults.setValue(newValue, forKey: Keys.showsTitles) }
    }
    
    
    /// Wether debug mode is enabled
    public var isDebugEnabled: Bool {
        get { defaults.bool(forKey: Keys.debugMode) }
        set {
            defaults.setValue(newValue, forKey: Keys.debugMode)
            self.objectWillChange.send()
        }
    }
    
    
    /// Beta Feature Flags
    public var betaflags: Set<BetaFlag> {
        get {
            let data = defaults.object(forKey: Keys.betaflags) as? [String] ?? [String]()
            let flags = data.filter({ BetaFlag(rawValue: $0) != nil }).map({BetaFlag(rawValue: $0)!})
            return Set(flags)
        }
        set {
            defaults.set(newValue.map({ $0.rawValue }), forKey: Keys.betaflags)
            objectWillChange.send()
        }
    }
    
    public var beta_singlePagemode: Bool {
        get {
            betaflags.isEnabled(.singlePageMode)
        }
        set {
            betaflags.set(.singlePageMode, to: newValue)
        }
    }
    
    public var beta_uglymode: Bool {
        get {
            betaflags.isEnabled(.uglymode)
        }
        set {
            betaflags.set(.uglymode, to: newValue)
        }
    }
    
    public var beta_coverRows: Bool {
        get {
            betaflags.isEnabled(.coverRows)
        }
        set {
            betaflags.set(.coverRows, to: newValue)
        }
    }
    
    public var beta_fetchCoverArt: Bool {
        get {
            betaflags.isEnabled(.coverArt)
        }
        set {
            betaflags.set(.coverArt, to: newValue)
        }
    }
    
    public enum BetaFlag: String, CaseIterable {
        case uglymode = "uglymode"
        case singlePageMode = "singlepagemode"
        case showsTitles = "showstitles"
        case coverRows = "coverrows"
        case coverArt = "coverart"
        
        public var label: LocalizedStringKey {
            return LocalizedStringKey("betaflags."+self.rawValue+".label")
        }
        
        public var description: LocalizedStringKey {
            return LocalizedStringKey("betaflags."+self.rawValue+".descr")
        }
        
        public static func availableFlags() -> [BetaFlag] {
            let config = Self.configuration()
            return Self.allCases.filter({ (config[$0] ?? false) })
        }
        
        public static func configuration() -> [BetaFlag: Bool] {
            return [
                .uglymode: true,
                .singlePageMode: false,
                .showsTitles: false,
                .coverRows: false,
                .coverArt: false
            ]
        }
    }
}


public extension Set where Element == PreferenceStore.BetaFlag {
    mutating func enable(_ flag: Element) {
        self.insert(flag)
    }
    
    mutating func disable(_ flag: Element) {
        self.remove(flag)
    }
    
    mutating func toggle(_ flag: Element) {
        if isEnabled(flag) {
            disable(flag)
        } else {
            enable(flag)
        }
    }
    
    mutating func set(_ flag: Element, to state: Bool) {
        if state != isEnabled(flag) {
            toggle(flag)
        }
    }
    
    func isEnabled(_ flag: Element) -> Bool {
        return self.contains(flag)
    }
}


public struct Version {
    public var description: String {
        if isTestFlight {
            return "[BETA] \(major).\(minor).\(patch) (\(build!))"
        } else {
            return "\(major).\(minor).\(patch)"
        }
    }
    
    
    public let major: Int
    public let minor: Int
    public let patch: Int
    public let build: Int?
    
    public var isTestFlight: Bool {
        return build != nil
    }
    
    public init() {
        let versionString = Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String
        let buildString = Bundle.main.infoDictionary!["CFBundleVersion"]! as! String
        
        self.major = Int(versionString.split(separator: ".")[0]) ?? 0
        self.minor = Int(versionString.split(separator: ".")[1]) ?? 0
        self.patch = Int(versionString.split(separator: ".")[2]) ?? 0
        
        self.build = Int(buildString) ?? 0
    }
}
