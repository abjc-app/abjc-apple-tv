//
//  AuthView.swift
//  ABJC
//
//  Created by Noah Kamara on 26.03.21.
//

import SwiftUI

struct AuthView: View {
    /// SessionStore EnvironmentObject
    @EnvironmentObject var session: SessionStore
    
    /// First Authentification Try failed
    @State var firstTryFailed: Bool = false
    
    var body: some View {
        NavigationView() {
            if !firstTryFailed {
                ProgressView()
            } else {
                ServerSelectionView()
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .onAppear(perform: authorize)
    }
    
    #warning("REMOVE BEFORE BUILD")
    func authorize() {
        let server = Jellyfin.Server("192.168.178.85", 8096, false)
        let client = Jellyfin.Client()
        
        API.authorize(server, client, "noah", "password") { result in
            switch result {
                case .success(let jellyfin):
                    session.setJellyfin(jellyfin)
                    
                case .failure(let error):
                    session.setAlert(
                        .auth,
                        "alerts.auth.failed",
                        "",
                        error
                    )
                    firstTryFailed = true
            }
        }
    }
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
