//
//  LibraryView.swift
//  ABJC
//
//  Created by Noah Kamara on 26.03.21.
//

import SwiftUI

struct LibraryView: View {
    /// SessionStore EnvironmentObject
    @EnvironmentObject var session: SessionStore
    
    var body: some View {
        TabView() {
            if session.preferences.showsWatchNowTab {
                WatchNowView()
                    .tabItem({ Text("library.watchnow.tablabel") })
                    .tag(0)
            }
            if session.preferences.showsMoviesTab {
                MediaCollectionView(.movie)
                    .tabItem({ Text("library.movies.tablabel") })
                    .tag(1)
            }

            if session.preferences.showsSeriesTab {
                MediaCollectionView(.series)
                    .tabItem({ Text("library.shows.tablabel") })
                    .tag(2)
            }

            if session.preferences.showsSearchTab {
                Text("Search") //SearchView()
                    .tabItem({
                        Text("library.search.tablabel")
                        Image(systemName: "magnifyingglass")
                    })
                    .tag(3)
            }
            
            PreferencesView()
                .tabItem({ Text("library.preferences.tablabel") })
                .tag(4)
        }
    }
}

struct LibraryView_Previews: PreviewProvider {
    static var previews: some View {
        LibraryView()
    }
}
