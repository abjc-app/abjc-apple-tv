//
//  MediaCollectionView.swift
//  ABJC
//
//  Created by Noah Kamara on 26.03.21.
//

import SwiftUI

extension LibraryView
{
    struct MediaCollectionView: View
    {
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        /// MediaType
        private let type: APIModels.MediaType?
        
        /// filtered items
        @State var itemMap: [APIModels.Genre: [String]] = [:]
        
        @State var items: [String: APIModels.MediaItem] = [:]
        @State var genres: [APIModels.Genre] = []
        
        /// Initializer
        /// - Parameter type: MediaType for API fetch
        public init(_ type: APIModels.MediaType? = nil) {
            self.type = type
        }
        
        var body: some View
        {
            NavigationView {
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(alignment: .leading) {
                        ForEach(genres, id:\.id) { genre in
                            MediaRowView(
                                genre.name,
                                 itemMap[genre]!.compactMap({ items[$0] })
                            )
                            Divider()
                        }
                    }
                }.edgesIgnoringSafeArea(.horizontal)
            }
            .onAppear(perform: load)
        }
        
        func load() {
            API.items(session.jellyfin!, type) { (result) in
                switch result {
                    case .failure(let error):
                        session.setAlert(.api, "Couldn't fetch Items", "Couldn't fetch Items of type \(String(describing: type?.rawValue))", error)
                    case .success(let items):
                        var itemStore: [APIModels.Genre: [String]] = [:]
                        var genres = Set<APIModels.Genre>()
                        
                        for item in items {
                            for genre in item.genres {
                                if itemStore.keys.contains(genre) {
                                    itemStore[genre]!.append(item.id)
                                } else {
                                    itemStore[genre] = [item.id]
                                }
                                
                                genres.insert(genre)
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.items = items.reduce(into: [String: APIModels.MediaItem]()) {
                                $0[$1.id] = $1
                            }
                            self.genres = Array(genres).sorted(by: { $0.name < $1.name})
                            self.itemMap = itemStore
                        }
                }
            }
        }
    }
    
    struct MediaCollectionView_Previews: PreviewProvider
    {
        static var previews: some View
        {
            MediaCollectionView()
        }
    }
}
