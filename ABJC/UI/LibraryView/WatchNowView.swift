//
//  WatchNowView.swift
//  ABJC
//
//  Created by Noah Kamara on 27.03.21.
//

import SwiftUI

extension LibraryView
{
    struct WatchNowView: View
    {
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        /// MediaType
        private let type: APIModels.MediaType?
        
        /// filtered items
        private var items: [APIModels.MediaItem] {
            if let type = type {
                return session.items.filter({ $0.type == type})
            } else {
                return session.items
            }
        }
        
        /// unique array of genres found in `items`
        private var genres: [APIModels.Genre] {
            let i = session.items.filter({ type == nil || $0.type == type }).flatMap({ ($0.genres) }).uniques
            return i
        }
        
        /// Initializer
        /// - Parameter type: MediaType for API fetch
        public init(_ type: APIModels.MediaType? = nil) {
            self.type = type
        }
        
        var body: some View
        {
            NavigationView {
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(alignment: .leading) {
//                        CoverRowView(nil, items)
                        ForEach(self.genres, id:\.id) { genre in
                            MediaRowView(
                                genre.name,
                                session.items.filter({ $0.genres.contains(genre) && (type == nil || $0.type == type)}))
                            Divider()
                        }
                    }
                }.edgesIgnoringSafeArea(.horizontal)
            }
//            .onAppear(perform: session.reloadItems)
        }
    }
    
    struct WatchNowView_Previews: PreviewProvider
    {
        static var previews: some View
        {
            WatchNowView()
        }
    }
}
