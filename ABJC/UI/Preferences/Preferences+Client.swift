//
//  Client.swift
//  ABJC
//
//  Created by Noah Kamara on 03.04.21.
//

import SwiftUI

extension PreferencesView {
    struct Client: View {
        
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        @State var betaflags: Set<PreferenceStore.BetaFlag> = Set<PreferenceStore.BetaFlag>()
        
        @State var showsWatchNowTab: Bool = false
        @State var showsMoviesTab: Bool = false
        @State var showsSeriesTab: Bool = false
        @State var showsSearchTab: Bool = false
        
        var body: some View {
            Form() {
                Section(header: Label("pref.client.ui.label", systemImage: "uiwindow.split.2x1")) {
                    InfoToggle("pref.client.showstitles.label", "pref.client.showstitles.descr", $session.preferences.showsTitles)
                }
                
                Section(header: Label("pref.client.tabs.label", systemImage: "photo.fill")) {
                    InfoToggle("pref.client.tabs.watchnow.label",
                               "pref.client.tabs.watchnow.descr",
                               $showsWatchNowTab)
                    
                    InfoToggle("pref.client.tabs.movies.label",
                               "pref.client.tabs.movies.descr",
                               $showsMoviesTab)
                    
                    InfoToggle("pref.client.tabs.series.label",
                               "pref.client.tabs.series.descr",
                               $showsSeriesTab)
                    
                    InfoToggle("pref.client.tabs.search.label",
                               "pref.client.tabs.search.descr",
                               $showsSearchTab)
                }
                
                Section(header: Label("pref.client.betaflags.label", systemImage: "exclamationmark.triangle.fill")) {
                    List(PreferenceStore.BetaFlag.availableFlags(), id: \.rawValue) { flag in
                        Button(action: {
                            DispatchQueue.main.async {
                                betaflags.toggle(flag)
                            }
                        }) {
                            HStack(alignment: .firstTextBaseline) {
                                Group() {
                                    if betaflags.contains(flag) {
                                        Image(systemName: "checkmark.circle.fill")
                                            .imageScale(.large)
                                    } else {
                                        Image(systemName: "circle")
                                            .foregroundColor(.clear)
                                    }
                                }
                                VStack(alignment: .leading) {
                                    Text(flag.label)
                                        .font(.headline)
                                        .bold()
                                    Text(flag.description)
                                        .font(.callout)
                                        .foregroundColor(.secondary)
                                }
                            }.padding()
                        }
                    }
                }
            }
            
            .onAppear(perform: loadSettings)
            .onDisappear(perform: storeSettings)
        }
        
        func loadSettings() {
            DispatchQueue.main.async {
                showsWatchNowTab = session.preferences.showsWatchNowTab
                showsMoviesTab = session.preferences.showsMoviesTab
                showsSeriesTab = session.preferences.showsSeriesTab
                showsSearchTab = session.preferences.showsSearchTab
                betaflags = session.preferences.betaflags
            }
        }
        
        func storeSettings() {
            print("STORING SETTINGS")
            session.preferences.showsWatchNowTab = showsWatchNowTab
            session.preferences.showsMoviesTab = showsMoviesTab
            session.preferences.showsSeriesTab = showsSeriesTab
            session.preferences.showsSearchTab = showsSearchTab
            session.preferences.betaflags = betaflags
        }
    }
    
    struct Client_Previews: PreviewProvider {
        static var previews: some View {
            Client()
        }
    }
}
