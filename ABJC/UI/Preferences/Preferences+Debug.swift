//
//  DebugMenu.swift
//  ABJC
//
//  Created by Noah Kamara on 03.04.21.
//

import SwiftUI
import URLImage

extension PreferencesView {
    struct DebugMenu: View {
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        
        public init() {}
        
        
        /// ViewBuilder body
        public var body: some View {
            Form() {
                Toggle("pref.debugmenu.debugmode.label", isOn: $session.preferences.isDebugEnabled)
                
                Section(header: Label("pref.debugmenu.images.label", systemImage: "photo.fill")) {
                    Button(action: {
                        URLImageService.shared.cleanup()
                    }) {
                        Text("pref.debugmenu.images.cleanupcache")
                            .textCase(.uppercase)
                    }
                    
                    Button(action: {
                        URLImageService.shared.removeAllCachedImages()
                    }) {
                        Text("pref.debugmenu.images.clearcache")
                            .textCase(.uppercase)
                    }
                }
            }
        }
    }
    
    struct DebugMenu_Previews: PreviewProvider {
        static var previews: some View {
            DebugMenu()
        }
    }

}
