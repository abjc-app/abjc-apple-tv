//
//  PreferencesView.swift
//  ABJC
//
//  Created by Noah Kamara on 03.04.21.
//

import SwiftUI

struct PreferencesView: View {
    
    /// SessionStore EnvironmentObject
    @EnvironmentObject var session: SessionStore
    
    @State var selection: Int? = 0
    
    var version: Version { session.preferences.version }
    
    var body: some View {
        NavigationView() {
            List(selection: $selection) {
                NavigationLink(destination: GeneralInfo()) {
                    Label("pref.general.label", systemImage: "server.rack")
                }.tag(0)
                
                NavigationLink(destination: Client()) {
                    Label("pref.client.label", systemImage: "tv")
                }.tag(1)

                NavigationLink(destination: DebugMenu()) {
                    Label("pref.debugmenu.label", systemImage: "exclamationmark.triangle")
                }.tag(2)

                
                // Report A Problem (FullScreenCover)
//                NavigationLink(destination: Report()) {
//                    Label("pref.report.label", systemImage: "xmark.octagon.fill")
//                }.tag(3)
                
//                NavigationLink(destination: About()) {
//                    Label("pref.about.label", systemImage: "exclamationmark.bubble")
//                }.tag(4)
                
//                NavigationLink(destination: PreferencesAbout()) {
//                    Label("pref.about.label", systemImage: "exclamationmark.bubble")
//                }.tag(3)
                
                Button(action: {
//                    self.session.logout()
                }) {
                    HStack {
                        Spacer()
                        Text("buttons.logout")
                            .bold()
                            .textCase(.uppercase)
                            .foregroundColor(.red)
                        
                        Spacer()
                    }
                }
            }
            
            
            VStack {
                Image("logo")
                HStack {
                    Text("App Version")
                    Text(version.description)
                }
            }
        }
    }
}

struct PreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        PreferencesView()
    }
}
