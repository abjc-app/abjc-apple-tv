//
//  CoverCardView.swift
//  ABJC
//
//  Created by Noah Kamara on 27.03.21.
//

import SwiftUI

extension LibraryView
{
    struct CoverCardView: View {
        
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        /// Item
        private let item: APIModels.MediaItem
        
        /// Initializer
        /// - Parameter item: Item
        public init(_ item: APIModels.MediaItem) {
            self.item = item
        }
        
        var body: some View {
            GeometryReader() { geo in
                ZStack(alignment: .leading) {
//                    background
                    Blur()
//                    HStack {
//                        image
//                            .clipShape(RoundedRectangle(cornerRadius: cornerRadius * 1.5, style: .continuous))
//                            .padding()
//                        info.padding()
//                    }
                }
                .clipped()
                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
            }.frame(height: 400)
        }
    }

//    struct CoverCardView_Previews: PreviewProvider {
//        static var previews: some View {
//            CoverCardView()
//        }
//    }
}
