//
//  CoverRowView.swift
//  ABJC
//
//  Created by Noah Kamara on 27.03.21.
//

import SwiftUI

extension LibraryView
{
    struct CoverRowView: View {
        /// SessionStore EnvironmentObject
        @EnvironmentObject var session: SessionStore
        
        /// Edge Insets
        private let edgeInsets = EdgeInsets(top: 20, leading: 80, bottom: 50, trailing: 80)
        
        /// Row Label
        private var label: LocalizedStringKey? = nil
        
        /// Row Items
        private var items: [APIModels.MediaItem]
        
        /// Initializer
        /// - Parameters:
        ///   - label: Localized Row Label
        ///   - items: Row Items
        public init(_ label: LocalizedStringKey?, _ items: [APIModels.MediaItem]) {
            self.label = label
            self.items = items
        }
        
        /// Initializer
        /// - Parameters:
        ///   - items: Row Items
        public init(_ items: [APIModels.MediaItem]) {
            self.items = items
        }
        
        /// ViewBuilder body
        public var body: some View {
            VStack(alignment: .leading, spacing: 0)
            {
                TabView
                {
                    ForEach(items, id:\.id)
                    { item in
                        NavigationLink(destination: Text(item.name) /*ItemPage(item)*/) {
                            CoverCardView(item)
                        }
                    }
                    .padding(edgeInsets)
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
//                .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
                
                .edgesIgnoringSafeArea(.horizontal)
                .frame(height: 400)
            }
            .edgesIgnoringSafeArea(.horizontal)
        }
    }

//    struct CoverRowView_Previews: PreviewProvider {
//        static var previews: some View {
//            CoverRowView()
//        }
//    }
}
