# [ABJC](http://abjc.tech/beta) is A Better Jellyfin Client

ABJC is currently in TestFlight beta and I'm planning to publish it on the App Store. 

> Want to test new builds as soon as they are available? 
> Join Beta development on [TestFlight](https://abjc.tech/beta)


## How to keep up with development progress

I am publishing new information on builds, beta progress, surveys and polls on different channels.

- Take a look at my [Website](https://abjc.tech)
- Sign up to my [Newsletter](https://abjc.tech/newsletter)
- Follow my [Twitter](https://twitter.com/abjc_app)

# The ABJC Issues Repo

The repo you are looking at is the ABJC Issues Repo. It exists as a public forum to gather users' feedback and discussions on bugs, missing features or desired enhancements. 

__As a developer I want to make ABJC perfect. I welcome all user ideas and opinions.__

## How to report a bug, missing feature, or suggest an enhancement

1. Open the `Issues` tab above. Search existing issues to see if someone else has already beat you to it. 
2. If someone has, feel free to vote that issue up or add your own feedback in a comment.
3. If not, click the `New Issue` button and explain. 
-  For bug reports, **please provide your OS version, ABJC version, and if possible, detailed steps to reproduce the bug**.

If you have multiple bugs or requests that are not closely related, please separate them into different issues.


This is the main repository for ABJC

[TestFlight](https://testflight.apple.com/join/AAHO5kPc)


## Issues
If you find any bugs or have feature requests please create an issue [here](https://bitbucket.org/abjc-app/abjc-apple-tv/issues/new)
